<?php

/**
 * The plugin bootstrap file
 * @since 1.0.0
 * @package WP_STH
 * 
 * @wordpress-plugin
 * Plugin name: Sync to HTML
 * Plugin URI: http://andru255.github.io/
 * Description: Plugin to listen post changes and trigger a process to static-html-output-plugin
 */

if(!defined('WPINC')) {
    die;
}

define('WP_STH_VERSION', '1.0');

if ( ! defined( 'WP_STH_FILE' ) ) {
    define('WP_STH_FILE', __FILE__);
}

if ( ! defined( 'WP_STH_PATH' ) ) {
	define('WP_STH_PATH', plugin_dir_path(WP_STH_FILE));
}

/**
 * The code that runs during plugin activation.
 */
function activate_wp_sth() {
    require_once WP_STH_PATH . 'includes/class-wp-sth-activator.php';
    Wp_Sth_Activator::activate();
}

/**
 * The code that runs during plugin activation.
 */
function deactivate_wp_sth() {
    require_once WP_STH_PATH . 'includes/class-wp-sth-deactivator.php';
    Wp_Sth_Deactivator::deactivate();
}

function show_error_notice() {
    $class = 'notice notice-error';
    $message = 'Para activar el plugin <b>sync to html</b>, primero instala el plugin <a href="https://es.wordpress.org/plugins/static-html-output-plugin/" target="_blank">WP Static HTML Output</a>';
    printf( '<div class="%1$s"><p>%2$s</p></div>', esc_attr( $class ), $message );
}

function enable_wp_sth_setup() {
    $plugin_dependency = 'static-html-output-plugin/wp-static-html-output.php';
    if(!is_plugin_active($plugin_dependency)) {
        add_action('admin_notices', 'show_error_notice');
    } else {
        register_activation_hook(WP_STH_FILE, 'activate_wp_sth');
        register_activation_hook(WP_STH_FILE, 'deactivate_wp_sth');
    }
}
add_action('admin_init', 'enable_wp_sth_setup');

/**
 * The core plugin class that is used to define i18n, admin-specific hooks,
 * and public-facing site hooks.
 */
require WP_STH_PATH. 'includes/class-wp-sth.php';


/**
 * Begins execution of the plugin
 * 
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does,
 * not affect the page life cycle.
 */
function run_wp_sth() {
    $plugin = new Wp_Sth();
    $plugin->run();
}
run_wp_sth();