<?php
/**
 * The file that defines the core plugin class
 * 
 * A class definition that includes attrs and functions used across both the
 * public-facing side of the site and the admin area.
 * 
 * @link http://andru255.github.io
 * @since 1.0.0
 *
 * @package Wp_Sth
 * @package Wp_Sth/includes
 */

/**
 * The core plugin class
 *
 * This is used to define i18, admin specific-hooks, and
 * public-facing site hooks.
 * 
 * Also maintains the unique identifier of this plugin as well
 * as the current version of the plugin.
 * 
 * @since 1.0.0
 * @package Wp_Sth
 * @subpackage Wp_Sth/includes
 * @author @andru255
 */


 class Wp_Sth {
     /**
      * The loader that's responsible for maintaining and registering all hooks that power
      * the plugin.
      * 
      * @since 1.0.0
      * @access protected
      * @var Wp_Sth_Loader $loader Maintains and registers all hooks for the plugin.
      */
      protected $loader;

      /**
       * The unique identifier of this plugin.
       * 
       * @since 1.0.0
       * @access protected
       * @var string $plugin_name The string used to uniquely identify this plugin.
       */
      protected $plugin_name;

      /**
       * The current version of the plugin.
       * 
       * @since 1.0.0
       * @access protected
       * @var string $version The current version of the plugin.
       */
      protected $version;

      /**
       * Define the core funcionality of the plugin.
       * 
       * Set the plugin name and the plugin version that can be used throughout the plugin.
       * Load the dependencies, define the locale, and set the hooks for the admin area and
       * the public-facing side of the site.
       * 
       * @since 1.0.0
       */
      public function __construct() {
        $this->plugin_name = 'wp-sth';
        $this->version = '1.0.0';
        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
      }


      /**
       * Load the required dependencies for this plugin
       * 
       * Include the following files that make up the plugin:
       * 
       * - Wp_Acp_Loader. Orchestrates the hooks of the plugin.
       * - Wp_Acp_i18n. Defines internationalization functionality.
       * - Wp_Acp_Admin. Defines all hooks for the admin area.
       * - Wp_Acp_Public. Defines all hooks for the public side of the site.
       * 
       * Create an instance of the loader which will be used to register the hooks
       * with WordPress.
       * 
       * @since 1.0.0
       * @access private
       */
      private function load_dependencies() {
        /**
         * The class responsible for orchestrating the actions and filters of the core plugin. 
         */
        require_once WP_STH_PATH. 'includes/class-wp-sth-loader.php';

        /**
         * The class responsible for defining i18n functionality of the plugin.
         */
        require_once WP_STH_PATH. 'includes/class-wp-sth-i18n.php';

        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once WP_STH_PATH. 'admin/class-wp-sth-admin.php';

        $this->loader = new Wp_Sth_Loader();
      }

      /**
       * Define the locate for this plugin for i18n.
       * 
       * Uses the Wp_Acp_i18n class in order to set the domain and to register the hook
       * with Wordpress.
       * 
       * @since 1.0.0
       * @access private
       */
      private function set_locale() {
          $plugin_i18n = new Wp_Sth_i18n();
          $plugin_i18n->set_domain($this->get_plugin_name());
          $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
      }

      /**
       * Register all the hooks related to the admin area funcionality
       * of the plugin.
       * 
       * @since 1.0.0
       * @access private
       */
      private function define_admin_hooks() {
          $plugin_admin = new Wp_Sth_Admin($this->get_plugin_name(), $this->get_version());
          $this->loader->add_action('post_submitbox_misc_actions', $plugin_admin, 'render_misc_action');
          $this->loader->add_action('wp_ajax_do_export', $plugin_admin, 'do_export');
          $this->loader->add_action('wp_ajax_do_send_to_ftp', $plugin_admin, 'do_send_to_ftp');
          $this->loader->add_action('wp_ajax_do_send_to_s3', $plugin_admin, 'do_send_to_s3');
          $this->loader->add_action('wp_ajax_sth_settings', $plugin_admin, 'sth_settings');
          $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
          $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');
          //$this->loader->add_filter('wpcf7_contact_form_properties', $plugin_admin, 'add_acp_properties', 10, 2);
          //$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
          //$this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

          // save/update our plugin options
          //$this->loader->add_action('admin_init', $plugin_admin, 'options_update');

          // Add menu item
          //$this->loader->add_action('admin_menu', $plugin_admin, 'add_plugin_admin_menu');

          // Add settings link to the plugin
          //$plugin_basename = plugin_basename(plugin_dir_path(__DIR__).$this->plugin_name.'.php');
          //$this->loader->add_filter('plugin_action_links_'.$plugin_basename, $plugin_admin, 'add_action_links');

          // Add to contact form plugin admin dashboard
          //$this->loader->add_filter('wpcf7_editor_panels', $plugin_admin, 'add_to_contact_form_tab');
      }

      /**
       * Run the loader to execute all of the hooks with WordPress.
       * 
       * @since 1.0.0
       */
      public function run() {
          return $this->loader->run();
      }

      /**
       * get's the plugin name
       * 
       * @since 1.0.0
       * @return string The name of the plugin
       */
      public function get_plugin_name() {
        return $this->plugin_name;
      }

      /**
       * The reference to the class that orchestrates the hooks with the plugin.
       * 
       * @since 1.0.0
       * @return Wp_Sth_Loader Orchestrates the hooks of the plugin.
       */
      public function get_loader() {
          return $this->loader;
      }

      /**
       * Retrieve the version number of the plugin.
       * 
       * @since 1.0.0
       * @return string the version number of the plugin
       */
      public function get_version() {
        return $this->version;
      }
 }