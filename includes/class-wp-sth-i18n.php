<?php

/**
 * Define the i18n functionality
 * 
 * Loads and defines the i18n files for this plugin
 * so that it is ready for translation.
 * 
 * @link https://andru255.github.io/
 * @since 1.0.0
 * 
 * @package Wp_Sth
 * @subpackage Wp_Sth/includes
 * @author @andru255
 */

class Wp_Sth_i18n {
    /**
     * The domain specified for this plugin.
     * 
     * @since 1.0.0
     * @access private
     * @var string $domain The domain identifier for this plugin.
     */
    private $domain;

    /**
     * Load the plugin text domain for translation.
     * 
     * @since 1.0.0
     */
    public function load_plugin_textdomain() {
        load_plugin_textdomain($this->domain, false, WP_STH_PATH.'/languages/');
    }

    /**
     * Set the domain equal to that of the specified domain.
     * 
     * @since 1.0.0
     * @param string $domain The domain that represents the locale of this plugin.
     */
    public function set_domain($domain) {
        $this->$domain = $domain;
    }

}