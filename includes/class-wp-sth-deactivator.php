<?php
/** 
 * Fired during plugin deactivation
 * 
 * @link http://andru255.github.io/
 * @since 1.0.0
 * 
 * @package Wp_Acp
 * @subpackage Wp_Acp/includes
 */

 /**
  * Fired during plugin deactivation
  * 
  * This class defines all code necessary to run during the plugin's deactivation.
  *
  * @since 1.0.0
  * @package Wp_Sth
  * @subpackage Wp_Sth/includes
  * @author @andru255
  */
class Wp_Sth_Deactivator {
    /**
     * @since 1.0.0
     */
    public static function deactivate() {

    }
}