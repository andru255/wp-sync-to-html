<?php
/** 
 * Fired during plugin activation
 * 
 * @link http://andru255.github.io/
 * @since 1.0.0
 * 
 * @package Wp_Sth
 * @subpackage Wp_Sth/includes
 */

 /**
  * Fired during plugin activation
  * 
  * This class defines all code necessary to run during the plugin's activation.
  *
  * @since 1.0.0
  * @package Wp_Sth
  * @subpackage Wp_Sth/includes
  * @author @andru255
  */
class Wp_Sth_Activator {
    /**
     * @since 1.0.0
     */
    public static function activate() {
    }
}