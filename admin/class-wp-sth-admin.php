<?php

/**
 * The admin-specific functionality of the plugin.
 * 
 * @link https://andru255.github.io
 * @since 1.0.0
 * @package Wp_Sth
 * @subpackage Wp_Sth/admin
 */

if (!class_exists('StaticHtmlOutput')) {
    require_once( ABSPATH . '/wp-content/plugins/static-html-output-plugin/library/StaticHtmlOutput.php' );
}

class PlusStaticHtmlOutput extends StaticHtmlOutput {
	public static function options_settings() {
		return self::$_instance->_options->getOption('static-export-settings');	
	}
}

class Wp_Sth_Admin {
    /**
     * The ID of this plugin.
     * 
     * @since 1.0.0
     * @access private
     * @var string $plugin_name The ID of this plugin.
     */
    private $plugin_name;

    /**
     * The version of this plugin.
     * 
     * @since 1.0.0
     * @access private
     * @var string $version The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set it's properties.
     * 
     * @since 1.0.0
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     */
    public function __construct($plugin_name, $version) {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the admin area.
     * 
     * @since 1.0.0
     */
    public function enqueue_styles() {
        wp_enqueue_style($this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/wp-sth-admin.css', array(), $this->version, 'all');
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     * 
     * @since 1.0.0
     */
    public function enqueue_scripts() {
      $config = array(
          "post_id" => get_the_ID()
      );
      wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/wp-sth-admin.js', array( 'jquery' ), $this->version, true );
      wp_localize_script($this->plugin_name, 'wp_sync_to_html', $config);
    }

    /**
     * Adding a view for show exporter action
     */
    public function render_misc_action() {
        include_once(WP_STH_PATH. 'admin/partials/wp-sth-admin-display.php');
    }

    /**
     * 
     * Add the method in the class StaticHtmlOutput
	public static function options_settings() {
		return self::$_instance->_options->getOption('static-export-settings');	
    }
    
    into the file:
    wp-content/plugins/static-html-output-plugin/library/StaticHtmlOutput.php

     */
    public function sth_settings() {
        $post_id = isset($_POST['id']) ? $_POST['id'] : '0';

        if ( $post_id == 0) {
            echo "NO POST DETECTED";
        }

        $permalink = get_permalink($post_id);
        $defaults = array('additionalUrls' => $permalink);

        $settings = PlusStaticHtmlOutput::options_settings();
        parse_str($settings, $array_settings);

        //merge
        $response = array_merge($array_settings, $defaults); 
        wp_send_json($response);
        wp_die();
    }

    /**
     * Make the export to zip file
     */
    public function do_export() {
        $plugin = StaticHtmlOutput::getInstance();
        try {
            $plugin->startExport();
            $plugin->crawlTheWordPressSite();
            $plugin->createTheArchive();
            wp_send_json_success("Exportación a zip satisfactoria");
        } catch(Exception $e) {
            wp_send_json_error($e->getMessage());
        }
        wp_die();
    }

    /*
     * Make the send of ftp
    */
    public function do_send_to_ftp() {
        $plugin = StaticHtmlOutput::getInstance();
        try {
            $plugin->ftpPrepareExport();
            $plugin->ftpTransferFiles();
            wp_send_json_success("enviado a ftp satisfactorio");
        } catch(Exception $e) {
            wp_send_json_error("Error en envio ftp: ". $e->getMessage());
        }
        wp_die();
    }

    /**
     * Make the send to s3
     */

    public function do_send_to_s3() {
        $plugin = StaticHtmlOutput::getInstance();
        try {
            $plugin->s3Export();
            wp_send_json_success("envío a s3 satisfactorio");
        } catch(Exception $e) {
            wp_send_json_error("Error en envio s3: ". $e->getMessage());
        }
        wp_die();
    }
}