<div class="widget-sync-to-html misc-pub-section">
    <div class="dashicons dashicons-migrate"></div>
    <a class="do-sync-to-html" href="#export_post" role="button">
        <span aria-hidden="true">Export to static HTML</span>
        <span class="screen-reader-text">Export to static HTML</span>
    </a>
    <div class="spinner"></div>
</div>
<div id="widget-sync-to-html-status-description" class="misc-pub-section">
    <ul></ul>
</div>