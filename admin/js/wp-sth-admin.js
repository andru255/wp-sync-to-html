/**
 * script here
 */
(function($){
    var STATUS_ELEMENTS = {
        'fail': $("<div class='dashicons dashicons-no'></div>"),
        'success': $("<div class='dashicons dashicons-yes'></div>"),
    };
    var $containerStatusDesc = $('#widget-sync-to-html-status-description ul');
    var $spinner = $('.widget-sync-to-html .spinner');
    var $btnDoSync = $(".do-sync-to-html");
    var $errorLog = $("<a class='do-sync-error-log' href='#'>Error encontrado</a>");
    var urlRegex = /http.*$/;

    var actionGetSettings = function(success, error) {
        var postId = wp_sync_to_html.post_id; 
        var data = {
            'action': 'sth_settings',
            'id': postId,
        };
        $.post(ajaxurl, data, success, error);
    };

    var doAction = function(settings, actionName) {
        var default_data = {
            'action': actionName
        };
        var data = Object.assign(settings, default_data);
        $.post(ajaxurl, data, function(dataResponse) {
            var $listItem = $("<li></li");
            var message = dataResponse.data;
            $listItem.html(message);
            if ( dataResponse.success ) {
                $listItem.prepend(STATUS_ELEMENTS.success);
            } else {
                $listItem.prepend(STATUS_ELEMENTS.fail);
            }
            $containerStatusDesc.append($listItem);
        }).fail(function(xhr, status, error){
            if (error) {
                var message = "Error encontrado en:" + actionName + "," + error;
                var $listItem = $("<li></li");
                $listItem.html(message);
                $listItem.prepend(STATUS_ELEMENTS.success);
                $containerStatusDesc.append($listItem);
            }
        }).always(function(){
            $spinner.removeClass("is-active");
        });
    };

    var getActionName = function(actionFlag) {
        var result = '';
        var actionsByFlags = {
            'retainStaticFiles': 'do_export',
            'sendViaFTP': 'do_send_to_ftp',
            'sendViaS3': 'do_send_to_s3' 
        };
        for (var keyName in actionsByFlags) {
            if (actionFlag == keyName) {
                result = actionsByFlags[keyName];
            } 
        }
        return result;
    };

    var getActions = function(settings, filterMethod) {
        var result = [];
        for (var keyName in settings){
            var actionName = filterMethod(keyName);
            if ( actionName != "" ) {
                result.push(actionName);
            }
        }
        return result;
    };

    $btnDoSync.click(function(evt){
        $spinner.addClass("is-active");
        actionGetSettings(function(settings){
            var actions = getActions(settings, getActionName);
            $containerStatusDesc.html("");
            for(var index=0; index < actions.length; index++) {
                console.log("send to:", actions[index]);
                doAction(settings, actions[index]);
            }
        });
        evt.preventDefault();
    });
}(jQuery));